0
1

2

0 a 0
0 ε 1
1 b 1
1 ε 2
2 c 2

<representation type="image/svg+xml">
<svg width="0" height="0" viewBox="0.00 0.00 326.00 86.80" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="graph1" class="graph">
<title>automaton</title>



<g id="MA==" class="node"><title>0</title>
<ellipse fill="white" stroke="black" cx="136" cy="60.8" rx="18" ry="18"></ellipse>
<text text-anchor="middle" x="136" y="65" font-family="Times,serif" font-size="14.00">0</text>
</g>

<g id="initialStateArrow" class="edge"><title>_begin-&gt;0</title>
<path fill="none" stroke="black" d="M80,60.79999923706055C95.33333333333333,60.79999923706055 110.66666666666667,60.79999923706055 108,60.79999923706055"></path>
<polygon fill="black" stroke="black" points="118,60.79999923706055 108,56.79999923706055 113,60.79999923706055 108,60.79999923706055 108,60.79999923706055 108,60.79999923706055 113,60.79999923706055 108,64.79999923706055 118,60.79999923706055 118,60.79999923706055"></polygon>
</g>

<g id="MA== MA==" class="edge"><title>0-&gt;0</title>
<path fill="none" stroke="black" d="M129.621,43.7627 C128.319,33.9421 130.445,24.8 136,24.8 C139.472,24.8 141.604,28.3711 142.398,33.4471"></path>
<polygon fill="black" stroke="black" points="145.897,33.769 142.379,43.7627 138.897,33.7565 145.897,33.769"></polygon>
<text text-anchor="middle" x="136" y="20.599999999999994" font-family="Times,serif" font-size="14.00">a</text>
</g>

<g id="MQ==" class="node"><title>1</title>
<ellipse fill="white" stroke="black" cx="216" cy="60.8" rx="18" ry="18"></ellipse>
<text text-anchor="middle" x="216" y="65" font-family="Times,serif" font-size="14.00">1</text>
</g>

<g id="MA== MQ==" class="edge"><title>0-&gt;1</title>
<path fill="none" stroke="black" d="M154.311,60.8 C164.106,60.8 176.578,60.8 187.665,60.8"></path>
<polygon fill="black" stroke="black" points="187.738,57.2999 197.738,60.8 187.738,64.2999 187.738,57.2999"></polygon>
<text text-anchor="middle" x="176" y="56.599999999999994" font-family="Times,serif" font-size="14.00">ε</text>
</g>

<g id="MQ== MQ==" class="edge"><title>1-&gt;1</title>
<path fill="none" stroke="black" d="M208.969,44.1359 C207.406,34.175 209.75,24.8 216,24.8 C220.004,24.8 222.405,28.6475 223.202,34.0318"></path>
<polygon fill="black" stroke="black" points="226.7,34.1967 223.031,44.1359 219.701,34.0781 226.7,34.1967"></polygon>
<text text-anchor="middle" x="216" y="20.599999999999994" font-family="Times,serif" font-size="14.00">b</text>
</g>

<g id="Mg==" class="node"><title>2</title>
<ellipse fill="white" stroke="black" cx="300" cy="60.8" rx="18" ry="18"></ellipse>
<ellipse fill="none" stroke="black" cx="300" cy="60.8" rx="22" ry="22"></ellipse>
<text text-anchor="middle" x="300" y="65" font-family="Times,serif" font-size="14.00">2</text>
</g>

<g id="MQ== Mg==" class="edge"><title>1-&gt;2</title>
<path fill="none" stroke="black" d="M234.39,60.8 C244.108,60.8 256.511,60.8 267.847,60.8"></path>
<polygon fill="black" stroke="black" points="267.85,57.2999 277.85,60.8 267.85,64.2999 267.85,57.2999"></polygon>
<text text-anchor="middle" x="256" y="56.599999999999994" font-family="Times,serif" font-size="14.00">ε</text>
</g>

<g id="Mg== Mg==" class="edge"><title>2-&gt;2</title>
<path fill="none" stroke="black" d="M291.994,40.2192 C290.886,29.9553 293.555,20.8 300,20.8 C304.129,20.8 306.708,24.5573 307.737,29.9564"></path>
<polygon fill="black" stroke="black" points="311.243,30.1309 308.006,40.2192 304.245,30.3143 311.243,30.1309"></polygon>
<text text-anchor="middle" x="300" y="16.599999999999994" font-family="Times,serif" font-size="14.00">c</text>
</g>
</g>
</svg>
</representation>
