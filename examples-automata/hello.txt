0
2
3

1
4

0 h 1
2 ε 3
3 l 3
3 o 4
1 e 2

<representation type="image/svg+xml">
<svg width="1200" height="1120" viewBox="49.146 -176.42 461.538 430.769" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="graph1" class="graph">
<title>automaton</title>



<g id="MA==" class="node"><title>0</title>
<ellipse fill="white" fill-opacity="0" stroke="black" cx="136" cy="56.8" rx="18" ry="18"></ellipse>
<text text-anchor="middle" x="136" y="61" font-family="Times,serif" font-size="14.00">0</text>
</g>

<g id="initialStateArrow" class="edge"><title>_begin-&gt;0</title>
<path fill="none" stroke="black" d="M80,56.79999923706055C95.33333333333333,56.79999923706055 110.66666666666667,56.79999923706055 108,56.79999923706055"></path>
<polygon fill="black" stroke="black" points="118,56.79999923706055 108,52.79999923706055 113,56.79999923706055 108,56.79999923706055 108,56.79999923706055 108,56.79999923706055 113,56.79999923706055 108,60.79999923706055 118,56.79999923706055 118,56.79999923706055"></polygon>
</g>

<g id="MQ==" class="node"><title>1</title>
<ellipse fill="white" fill-opacity="0" stroke="black" cx="216.92307692307693" cy="-31.661538461538456" rx="18" ry="18"></ellipse>
<ellipse fill="white" fill-opacity="0" stroke="black" cx="216.92307692307693" cy="-31.661538461538456" rx="22" ry="22"></ellipse>
<text text-anchor="middle" x="216.92307692307693" y="-27.461538461538453" font-family="Times,serif" font-size="14.00">1</text>
</g>

<g id="MA== MQ==" class="edge"><title>0-&gt;1</title>
<path style="" fill="none" stroke="black" d="M148.149,43.5188 C163.874,26.329 179.599,9.13928 195.324,-8.05046"></path>
<polygon style="" fill="black" stroke="black" points="197.907,-5.68807 202.074,-15.4289 192.742,-10.4129 197.907,-5.68807"></polygon>
<text style="" text-anchor="middle" x="171.73675537109375" y="12.734154224395752" font-family="Times,serif" font-size="14.00">h</text>
</g>

<g id="Mg==" class="node"><title>2</title>
<ellipse fill="white" fill-opacity="0" stroke="black" cx="336.61538461538464" cy="-48.2" rx="18" ry="18"></ellipse>
<text text-anchor="middle" x="336.61538461538464" y="-44" font-family="Times,serif" font-size="14.00">2</text>
</g>

<g id="Mw==" class="node"><title>3</title>
<ellipse fill="white" fill-opacity="0" stroke="black" cx="415.46153846153845" cy="-22.815384615384616" rx="18" ry="18"></ellipse>
<text text-anchor="middle" x="415.46153846153845" y="-18.615384615384613" font-family="Times,serif" font-size="14.00">3</text>
</g>

<g id="Mg== Mw==" class="edge"><title>2-&gt;3</title>
<path style="" fill="none" stroke="black" d="M353.749,-42.6837 C365.436,-38.9212 377.122,-35.1588 388.809,-31.3963"></path>
<polygon style="" fill="black" stroke="black" points="389.881,-34.7279 398.328,-28.3317 387.736,-28.0647 389.881,-34.7279"></polygon>
<text style="" text-anchor="middle" x="371.279052734375" y="-42.03999328613281" font-family="Times,serif" font-size="14.00">ε</text>
</g>

<g id="Mw== Mw==" class="edge"><title>3-&gt;3</title>
<path style="" fill="none" stroke="black" d="M408.431,-39.4795 C406.868,-49.4404 409.212,-58.8154 415.462,-58.8154 C419.466,-58.8154 421.867,-54.9679 422.664,-49.5836"></path>
<polygon style="" fill="black" stroke="black" points="426.162,-49.4187 422.493,-39.4795 419.163,-49.5373 426.162,-49.4187"></polygon>
<text style="" text-anchor="middle" x="415.46153846153845" y="-63.01538461538462" font-family="Times,serif" font-size="14.00">l</text>
</g>

<g id="NA==" class="node"><title>4</title>
<ellipse fill="white" fill-opacity="0" stroke="black" cx="322.53846153846155" cy="119.49230769230769" rx="18" ry="18"></ellipse>
<ellipse fill="white" fill-opacity="0" stroke="black" cx="322.53846153846155" cy="119.49230769230769" rx="22" ry="22"></ellipse>
<text text-anchor="middle" x="322.53846153846155" y="123.6923076923077" font-family="Times,serif" font-size="14.00">4</text>
</g>

<g id="Mw== NA==" class="edge"><title>3-&gt;4</title>
<path style="" fill="none" stroke="black" d="M405.62,-7.74391 C383.758,25.7369 361.896,59.2177 340.034,92.6986"></path>
<polygon style="" fill="black" stroke="black" points="342.965,94.6122 334.567,101.072 337.103,90.785 342.965,94.6122"></polygon>
<text style="" text-anchor="middle" x="372.82716369628906" y="37.47733187675476" font-family="Times,serif" font-size="14.00">o</text>
</g>

<g id="MQ== Mg==" class="edge"><title>1-&gt;2</title>
<path style="" fill="none" stroke="black" d="M238.716,-34.6728 C262.104,-37.9044 285.491,-41.1359 308.879,-44.3675"></path>
<polygon style="" fill="black" stroke="black" points="309.358,-40.9005 318.785,-45.7363 308.4,-47.8346 309.358,-40.9005"></polygon>
<text style="" text-anchor="middle" x="273.7974624633789" y="-44.5201473236084" font-family="Times,serif" font-size="14.00">e</text>
</g>
</g>
</svg>
</representation>
