s1
s2
s3
s4
s5

H

s1 1/0;> s2
s1 0/-;- H
s2 1/1;> s2
s2 0/0;> s3
s3 1/1;> s3
s3 0/1;< s4
s4 1/1;< s4
s4 0/0;< s5
s5 0/1;> s1
s5 1/1;< s5

<representation type='image/svg+xml'>
<svg width="1680" height="560" viewBox="-349.275 -221.367 1680 560" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g class="graph" data-id="graph0">
<title>automaton</title>



<g class="node" data-id="czE=">
<title>s1</title>
<ellipse fill="white" stroke="#000000" cx="135.2621" cy="118.4205" rx="19.7419" ry="19.7419" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="135.2621" y="122.62049999999999" font-family="Times,serif" font-size="14.00" fill="#000000">s1</text>
</g>

<g class="initialStateArrow" data-id="initialStateArrow" transform="rotate(0)">
<title>_begin-&gt;s1</title>
<path fill="none" stroke="#000000" d="M77.52020072937012,118.42050170898438 C96.85353406270345,118.42050170898438 106.18686739603679,118.42050170898438 105.52020072937012,118.42050170898438"></path>
<polygon fill="#000000" stroke="#000000" points="115.52020072937012,118.42050170898438 105.52020072937012,114.42050170898438 110.52020072937012,118.42050170898438 105.52020072937012,118.42050170898438 105.52020072937012,118.42050170898438 105.52020072937012,118.42050170898438 110.52020072937012,118.42050170898438 105.52020072937012,122.42050170898438 115.52020072937012,118.42050170898438 115.52020072937012,118.42050170898438"></polygon>
</g>

<g class="node" data-id="czI=">
<title>s2</title>
<ellipse fill="white" stroke="#000000" cx="247.8849" cy="58.420500000000004" rx="19.7419" ry="19.7419" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="247.8849" y="62.62050000000001" font-family="Times,serif" font-size="14.00" fill="#000000">s2</text>
</g>

<g class="edge" data-id="czE= czI=">
<title>s1-&gt;s2</title>
<path fill="none" stroke="black" d="M 152.4334 108.695 C 158.8298 105.1093 166.1664 101.0402 172.8827 97.4205 C 188.7334 88.8779 206.6176 79.5659 220.9458 72.184"></path>
<polygon fill="black" stroke="black" points="219.734,68.8716 230.228,67.415 222.933,75.098 219.734,68.8716"></polygon>
<text text-anchor="middle" x="190.37989807128906" y="75.22049713134766" font-family="Times,serif" font-size="14.00">1/0;&gt;</text>
</g>

<g class="node" data-id="SA==">
<title>H</title>
<ellipse fill="white" stroke="#000000" cx="247.8849" cy="118.4205" rx="18.0143" ry="18.0143" fill-opacity="0"></ellipse>
<ellipse fill="white" stroke="#000000" cx="247.8849" cy="118.4205" rx="22.0157" ry="22.0157" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="247.8849" y="122.62049999999999" font-family="Times,serif" font-size="14.00" fill="#000000">H</text>
</g>

<g class="edge" data-id="czE= SA==">
<title>s1-&gt;H</title>
<path fill="none" stroke="black" d="M 155.1376 118.4205 C 171.7613 118.4205 195.8093 118.4205 215.2523 118.4205"></path>
<polygon fill="black" stroke="black" points="215.504,114.92 225.504,118.421 215.504,121.92 215.504,114.92"></polygon>
<text text-anchor="middle" x="190.37989807128906" y="114.22049713134766" font-family="Times,serif" font-size="14.00">0/-;-</text>
</g>

<g class="edge" data-id="czI= czI=">
<title>s2-&gt;s2</title>
<path fill="none" stroke="black" d="M 238.3928 41.00630000000001 C 236.2834 30.59700000000001 239.4474 20.80000000000001 247.8849 20.80000000000001 C 253.4221 20.80000000000001 256.68809999999996 25.019200000000012 257.683 30.861799999999988"></path>
<polygon fill="black" stroke="black" points="261.177,31.1164 257.377,41.0063 254.18,30.9053 261.177,31.1164"></polygon>
<text text-anchor="middle" x="247.88490295410156" y="16.600000381469727" font-family="Times,serif" font-size="14.00">1/1;&gt;</text>
</g>

<g class="node" data-id="czM=">
<title>s3</title>
<ellipse fill="white" stroke="#000000" cx="357.4012" cy="86.4205" rx="19.7419" ry="19.7419" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="357.4012" y="90.6205" font-family="Times,serif" font-size="14.00" fill="#000000">s3</text>
</g>

<g class="edge" data-id="czI= czM=">
<title>s2-&gt;s3</title>
<path fill="none" stroke="black" d="M 266.9612 63.297799999999995 C 283.896 67.6275 308.8956 74.0191 328.3215 78.9857"></path>
<polygon fill="black" stroke="black" points="329.355,75.6372 338.176,81.5052 327.621,82.4191 329.355,75.6372"></polygon>
<text text-anchor="middle" x="303.8367004394531" y="65.22049713134766" font-family="Times,serif" font-size="14.00">0/0;&gt;</text>
</g>

<g class="edge" data-id="czM= czM=">
<title>s3-&gt;s3</title>
<path fill="none" stroke="black" d="M 348.2605 69.0063 C 346.2293 58.596999999999994 349.2762 48.8 357.4012 48.8 C 362.7332 48.8 365.8783 53.0192 366.8364 58.8618"></path>
<polygon fill="black" stroke="black" points="370.331,59.1122 366.542,69.0063 363.334,58.9089 370.331,59.1122"></polygon>
<text text-anchor="middle" x="357.4012145996094" y="44.599998474121094" font-family="Times,serif" font-size="14.00">1/1;&gt;</text>
</g>

<g class="node" data-id="czQ=">
<title>s4</title>
<ellipse fill="white" stroke="#000000" cx="464.5301" cy="117.4205" rx="19.7419" ry="19.7419" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="464.5301" y="121.62049999999999" font-family="Times,serif" font-size="14.00" fill="#000000">s4</text>
</g>

<g class="edge" data-id="czM= czQ=">
<title>s3-&gt;s4</title>
<path fill="none" stroke="black" d="M 376.5537 91.9627 C 393.0064 96.7237 416.9707 103.6582 435.7347 109.088"></path>
<polygon fill="black" stroke="black" points="436.91,105.785 445.543,111.926 434.965,112.509 436.91,105.785"></polygon>
<text text-anchor="middle" x="410.9656066894531" y="93.22049713134766" font-family="Times,serif" font-size="14.00">0/1;&lt;</text>
</g>

<g class="edge" data-id="czQ= czQ=">
<title>s4-&gt;s4</title>
<path fill="none" stroke="black" d="M 455.3895 100.0063 C 453.3582 89.597 456.4051 79.8 464.5301 79.8 C 469.8621 79.8 473.0072 84.0192 473.9653 89.8618"></path>
<polygon fill="black" stroke="black" points="477.46,90.1122 473.671,100.006 470.463,89.9089 477.46,90.1122"></polygon>
<text text-anchor="middle" x="464.53009033203125" y="75.5999984741211" font-family="Times,serif" font-size="14.00">1/1;&lt;</text>
</g>

<g class="node" data-id="czU=">
<title>s5</title>
<ellipse fill="white" stroke="#000000" cx="571.659" cy="152.4205" rx="19.7419" ry="19.7419" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="571.659" y="156.6205" font-family="Times,serif" font-size="14.00" fill="#000000">s5</text>
</g>

<g class="edge" data-id="czQ= czU=">
<title>s4-&gt;s5</title>
<path fill="none" stroke="black" d="M 483.1905 123.5171 C 499.7562 128.9292 524.2108 136.9188 543.2132 143.127"></path>
<polygon fill="black" stroke="black" points="544.434,139.844 552.853,146.276 542.26,146.498 544.434,139.844"></polygon>
<text text-anchor="middle" x="518.0946044921875" y="126.22049713134766" font-family="Times,serif" font-size="14.00">0/0;&lt;</text>
</g>

<g class="edge" data-id="czU= czE=">
<title>s5-&gt;s1</title>
<path fill="none" stroke="black" d="M 552.435 157.04840000000002 C 531.0929 161.7738 495.5585 168.4205 464.5301 168.4205 C 247.8849 168.4205 247.8849 168.4205 247.8849 168.4205 C 215.1689 168.4205 181.3497 150.5773 159.4205 136.20929999999998"></path>
<polygon fill="black" stroke="black" points="157.365,139.044 151.068,130.524 161.304,133.257 157.365,139.044"></polygon>
<text text-anchor="middle" x="357.4012145996094" y="164.2205047607422" font-family="Times,serif" font-size="14.00">0/1;&gt;</text>
</g>

<g class="edge" data-id="czU= czU=">
<title>s5-&gt;s5</title>
<path fill="none" stroke="black" d="M 562.5184 135.0063 C 560.4872 124.59700000000001 563.534 114.8 571.659 114.8 C 576.9911 114.8 580.1361 119.0192 581.0942 124.8618"></path>
<polygon fill="black" stroke="black" points="584.589,125.112 580.8,135.006 577.591,124.909 584.589,125.112"></polygon>
<text text-anchor="middle" x="571.6589965820312" y="110.5999984741211" font-family="Times,serif" font-size="14.00">1/1;&lt;</text>
</g>
</g>
</svg>
</representation>
