{
  "title": "Exam2019",
  "author": "FabienPORTAS",
  "date": "18 July 2019",
  "description": "Examen final de première session 2018-2019 de l'UE INF302.",
  "questions": [{
    "type": "mcq",
    "instruction": "Soit un AEFD $A$ sur un alphabet $\\Sigma$ dont la fonction de transition est notée $\\delta$. Soit $u \\in \\Sigma^*$ dont l’exécution sur $A$ <u>n’est pas définie</u>.<br>\nIndiquer les affirmations que l’on peut déduire : ",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "1",
    "possibilities": [{
      "id": "a",
      "html": "$u$ n'est pas accepté par $A$."
    }, {
      "id": "b",
      "html": "$A$ n'est pas complet."
    }, {
      "id": "c",
      "html": "$u \\notin \\mathcal{L}(A)$."
    }, {
      "id": "d",
      "html": "Il y a un état $q$ de $A$ et un symbole $e$ de $\\Sigma$ tel que $(q,e)$ n'est pas dans le domaine de $\\delta$."
    }, {
      "id": "e",
      "html": "$A$ a au moins un état non accessible."
    }, {
      "id": "f",
      "html": "$A$ a au moins un état accessible."
    }, {
      "id": "g",
      "html": "$A$ a au moins un état non co-accessible."
    }, {
      "id": "h",
      "html": "$A$ a au moins un état co-accessible."
    }, {
      "id": "i",
      "html": "$A$ n'a pas d'état accepteur."
    }],
    "answers": ["a", "b", "c", "d", "f"],
    "singleChoice": false
  }, {
    "type": "mcq",
    "instruction": "Soient $L_1$ et $L_2$ deux langages à états. Nous notons $L_1^R$ et $L_2^R$ les langages miroirs de $L_1$ et $L_2$, respectivement. <br>Indiquer les langages à états.",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "1",
    "possibilities": [{
      "id": "a",
      "html": "$L_1^R \\cup L_2^R$"
    }, {
      "id": "b",
      "html": "$\\text{Pref}(L_1) \\backslash \\text{Pref}(L_2)$"
    }, {
      "id": "c",
      "html": "$\\text{Pref}(L_1) \\cup \\text{Pref}(L_2)$"
    }, {
      "id": "d",
      "html": "$L_1 · L_2$"
    }, {
      "id": "e",
      "html": "$L_1 \\cap L_2$"
    }, {
      "id": "f",
      "html": "$\\text{Pref}(L_1) \\cap \\text{Pref}(L_2)$"
    }, {
      "id": "g",
      "html": "$L_1^R \\backslash L_2^R$"
    }, {
      "id": "h",
      "html": "$L_1 \\backslash L_2$"
    }, {
      "id": "i",
      "html": "$L_1^R · L_2^R$"
    }, {
      "id": "j",
      "html": "$L_1^R \\cap L_2^R$"
    }, {
      "id": "k",
      "html": "$\\text{Pref}(L_1) · \\text{Pref}(L_2)$"
    }, {
      "id": "l",
      "html": "$L_1 \\cup L_2$"
    }],
    "answers": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"],
    "singleChoice": false
  }, {
    "type": "mcq",
    "instruction": "Soit $\\Sigma$ un alphabet tel que $\\{a, b\\} \\subseteq \\Sigma$.<br>\nIndiquer les langages réguliers :",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "1",
    "possibilities": [{
      "id": "a",
      "html": "L'ensemble des mots du dictionnaire français."
    }, {
      "id": "b",
      "html": "$\\left\\lbrace a^n · b^m · a^{n+m} \\mid n, m \\in \\mathbb{N} \\right\\rbrace$"
    }, {
      "id": "c",
      "html": "$\\left\\lbrace a^n · b^n \\mid n \\in \\mathbb{N} \\right\\rbrace$"
    }, {
      "id": "d",
      "html": "$\\{ w · w · w \\mid w \\in \\Sigma^* \\} $"
    }, {
      "id": "e",
      "html": "$\\{ w \\mid w \\in \\Sigma^* \\}$"
    }, {
      "id": "f",
      "html": "$\\left\\lbrace a^n · b^m \\mid n, m \\in \\mathbb{N} \\right\\rbrace$"
    }, {
      "id": "g",
      "html": "$\\left\\lbrace a^n · b^m \\mid n, m \\in \\mathbb{N}, n \\geqslant m \\right\\rbrace$"
    }],
    "answers": ["a", "e", "f"],
    "singleChoice": false
  }, {
    "type": "mcq",
    "instruction": "Soient $A$ et $B$ deux AEFDs complets et $P$ l’automate produit de $A$ et $B$. Les ensembles d’états de ces automates sont respectivement $Q_A$, $Q_B$ et $Q_P$ . Les ensembles d’états\naccepteurs/finaux de ces automates sont respectivement $F_A$ , $F_B$ et $F_P$.<br>\nIndiquer les affirmations que l’on peut déduire.",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "1",
    "possibilities": [{
      "id": "a",
      "html": "$\\vert Q_P \\vert \\leqslant \\vert Q_A \\vert \\times \\vert Q_B \\vert$."
    }, {
      "id": "b",
      "html": "$\\vert F_P \\vert \\leqslant \\vert F_A \\vert \\times \\vert F_B \\vert$."
    }, {
      "id": "c",
      "html": "Si un mot est accepté par $P$, alors il est accepté par $A$ et par $B$."
    }, {
      "id": "d",
      "html": "$\\vert Q_P \\vert > \\vert Q_A \\vert \\times \\vert Q_B \\vert$."
    }, {
      "id": "e",
      "html": "Si pour $A$ ou $B$ (ou les deux) tous les états sont accessibles, alors les états de $P$ sont tous accessibles."
    }, {
      "id": "f",
      "html": "$\\vert F_P \\vert > \\vert F_A \\vert \\times \\vert F_B \\vert$."
    }, {
      "id": "g",
      "html": "Si pour $A$ et $B$ tous les états sont accessibles, alors les états de $P$ sont tous accessibles."
    }, {
      "id": "h",
      "html": "Si un mot est accepté par $A$ et par $B$, alors il est accepté par $P$."
    }, {
      "id": "i",
      "html": "$P$ est complet."
    }],
    "answers": ["a", "b", "c", "g", "h", "i"],
    "singleChoice": false
  }, {
    "type": "mcq",
    "instruction": "Soit un AEFD $A$ sur un alphabet $\\Sigma$ dont la fonction de transition est notée $\\delta$. Soit $u \\in \\Sigma^*$ dont l’exécution sur $A$ <u>est définie</u>.<br>\nIndiquer les affirmations que l’on peut déduire.\n",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "1",
    "possibilities": [{
      "id": "a",
      "html": "Les états de $A$ sont tous accessibles."
    }, {
      "id": "b",
      "html": "$u \\in \\mathcal{L}(A)$"
    }, {
      "id": "c",
      "html": "$u$ est accepté par $A$."
    }, {
      "id": "d",
      "html": "$A$ est complet."
    }, {
      "id": "e",
      "html": "Les états de $A$ sont tous co-accessibles."
    }, {
      "id": "f",
      "html": "$A$ a des états accepteurs."
    }],
    "answers": [],
    "singleChoice": false
  }, {
    "type": "mcq",
    "instruction": "Soient $e$ et $e'$ deux expressions régulières.<br>Indiquer les équivalences sémantiques entre expressions régulières qui sont correctes.",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "1",
    "possibilities": [{
      "id": "a",
      "html": "$e + \\emptyset \\equiv \\emptyset + e \\equiv \\emptyset$"
    }, {
      "id": "b",
      "html": "$(\\emptyset)^* \\equiv \\emptyset$"
    }, {
      "id": "c",
      "html": "$(\\emptyset)^* \\equiv \\epsilon$"
    }, {
      "id": "d",
      "html": "$\\epsilon · \\emptyset \\equiv \\emptyset · \\epsilon \\equiv \\emptyset $"
    }, {
      "id": "e",
      "html": "$e^* + \\epsilon \\equiv \\epsilon + e^* \\equiv e^*$"
    }, {
      "id": "f",
      "html": "$e + \\epsilon \\equiv \\epsilon + e \\equiv \\epsilon$"
    }, {
      "id": "g",
      "html": "$e + \\epsilon \\equiv \\epsilon + e \\equiv e$"
    }, {
      "id": "h",
      "html": "$e^* · \\epsilon \\equiv \\epsilon · e^* \\equiv e^*$"
    }, {
      "id": "i",
      "html": "$e · \\emptyset \\equiv \\emptyset · e \\equiv e$"
    }, {
      "id": "j",
      "html": "$e + \\emptyset \\equiv \\emptyset + e \\equiv e$"
    }, {
      "id": "k",
      "html": "$e · \\epsilon \\equiv \\epsilon · e \\equiv \\epsilon$"
    }, {
      "id": "l",
      "html": "$e · \\epsilon \\equiv \\epsilon · e \\equiv e$"
    }],
    "answers": ["c", "d", "e", "h", "j", "l"],
    "singleChoice": false
  }, {
    "type": "mcq",
    "instruction": "Nous considérons un alphabet $\\Sigma$ tel que $ \\{a, b\\} \\subseteq \\Sigma$ et quelques langages (ensemble de mots) définis sur $\\Sigma$. <br>Indiquer les langages à états.\n",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "1",
    "possibilities": [{
      "id": "a",
      "html": "Un langage reconnu par un AEFD non-complet sur $\\Sigma$."
    }, {
      "id": "b",
      "html": "L’ensemble des mots qui ne contiennent pas plus de deux occurrences du symbole $b$."
    }, {
      "id": "c",
      "html": "L’ensemble des mots qui contiennent deux fois plus d’occurrences de $a$ que d’occurrences de $b$."
    }, {
      "id": "d",
      "html": "L’ensemble des préfixes du langage universel."
    }, {
      "id": "e",
      "html": "Un langage reconnu par un AEFND non-complet sur $\\Sigma$."
    }, {
      "id": "f",
      "html": "Un langage reconnu par un $\\epsilon$-AEFND non-complet sur $\\Sigma$"
    }, {
      "id": "g",
      "html": "L’ensemble des mots qui contiennent autant d’occurrences de $a$ que d’occurrences de $b$ et pas plus de 42 occurrences du symbole $a$."
    }, {
      "id": "h",
      "html": "Un langage reconnu par un AEFD sur $\\Sigma$."
    }, {
      "id": "i",
      "html": "L’ensemble des mots qui contiennent autant d’occurrences de $a$ que d’occurrences de $b$."
    }, {
      "id": "j",
      "html": "Le langage universel."
    }, {
      "id": "k",
      "html": "Un langage reconnu par un AEFND sur $\\Sigma$."
    }, {
      "id": "l",
      "html": "Un langage reconnu par un $\\epsilon$-AEFND sur $\\Sigma$."
    }, {
      "id": "m",
      "html": "L’ensemble des mots qui contiennent deux fois plus d’occurrences de $a$ que d’occurrences de $b$ et pas plus de 42 occurrences du symbole $a$."
    }, {
      "id": "n",
      "html": "Le langage vide."
    }],
    "answers": ["a", "b", "d", "e", "f", "g", "h", "j", "k", "l", "m", "n"],
    "singleChoice": false
  }, {
    "type": "mcq",
    "instruction": " Soit $L$ un langage à états sur l’alphabet $\\{a, b, c\\}$. Nous considérons le langage $L_h$ sur l’alphabet $\\{0, \\dots , 9\\}$ obtenu en remplaçant, dans tous les mots de $L$, chaque occurence de $a$ par\n$4 \\,· 2$, chaque occurence de $b$ par $\\epsilon$ et chaque occurrence de $c$ par $5\\, · 6$.\n",
    "isInstructionHTML": false,
    "wordingDetails": [],
    "point": "0.5",
    "possibilities": [{
      "id": "a",
      "html": "$L_h$ est nécéssairement un langage à états."
    }, {
      "id": "b",
      "html": "$L_h$ n’est pas nécessairement un langage à états."
    }],
    "answers": ["a"],
    "singleChoice": true
  }, {
    "type": "mcq",
    "instruction": "Considérons l’automate représenté ci-dessous sur l’alphabet $\\Sigma = \\{a,b\\}$.<br>L’automate correct résultant de l’algorithme de minimisation est celui représenté dans :\n",
    "isInstructionHTML": true,
    "wordingDetails": [{
      "aType": "Automaton",
      "content": "0\n1\n2\n3\n5\nS\n\n4\n6\n\n0 a 1\n0 b 5\n1 a 2\n1 b S\n2 a 3\n2 b S\n3 a 0\n3 b 4\n4 a 6\n4 b 4\n5 a 0\n5 b 4\n6 a 6\n6 b 6\n\n"
    }],
    "point": "2",
    "possibilities": [{
      "id": "a",
      "html": "",
      "automaton": {
        "states": [0, 1, 2, 3, 4, 5],
        "finalStates": [0, 2],
        "transitions": [
          [0, "a", 3],
          [0, "b", 1],
          [3, "a", 4],
          [3, "b", 5],
          [4, "b", 5],
          [4, "a", 1],
          [5, "a", 5],
          [5, "b", 5],
          [1, "a", 0],
          [1, "b", 2],
          [2, "a", 2],
          [2, "b", 2]
        ]
      }
    }, {
      "id": "b",
      "html": "",
      "automaton": {
        "states": [0, 1, 2, 3, 4, 5, 6],
        "finalStates": [4],
        "transitions": [
          [0, "a", 1],
          [0, "b", 3],
          [1, "a", 2],
          [1, "b", 6],
          [2, "a", 3],
          [2, "b", 6],
          [3, "a", 0],
          [3, "b", 4],
          [4, "a", 4],
          [4, "b", 4],
          [4, "b", 5],
          [5, "b", 6],
          [6, "a", 6],
          [6, "b", 6],
          [5, "a", 0],
          [5, "b", 0]
        ]
      }
    }, {
      "id": "c",
      "html": "",
      "automaton": {
        "states": [0, 1, 2, 3, 4, 5],
        "finalStates": [5],
        "transitions": [
          [0, "a", 1],
          [0, "a", 4],
          [0, "b", 4],
          [1, "a", 2],
          [1, "b", 3],
          [3, "a", 3],
          [2, "b", 3],
          [2, "a", 4],
          [4, "a", 0],
          [4, "b", 5],
          [5, "a", 5],
          [5, "b", 5]
        ]
      }
    }, {
      "id": "d",
      "html": "",
      "automaton": {
        "states": [0, 1, 2, 3, 4, 5],
        "finalStates": [2],
        "transitions": [
          [0, "a", 3],
          [0, "b", 1],
          [3, "a", 4],
          [3, "b", 5],
          [4, "b", 5],
          [4, "a", 1],
          [5, "a", 5],
          [5, "b", 5],
          [1, "a", 0],
          [1, "b", 2],
          [2, "a", 2],
          [2, "b", 2]
        ]
      }
    }],
    "answers": ["d"],
    "singleChoice": false
  }, {
    "type": "mcq",
    "instruction": " Considérons l’$\\epsilon$-AEFND suivant.<br>Le déterminisé de\ncet automate est celui représenté dans :\n",
    "isInstructionHTML": true,
    "wordingDetails": [{
      "aType": "Automaton",
      "content": "0\n1\n2\n3\n4\n\n5\n\n0 a 1\n0 b 0\n0 b 3\n0 \\e 2\n1 a 1\n1 a 5\n2 \\e 1\n3 a 4\n3 b 0\n4 b 1\n4 \\e 3\n5 a 5\n5 b 5\n5 \\e 2\n\n"
    }],
    "point": "2",
    "possibilities": [{
      "id": "a",
      "html": "",
      "automaton": {
        "states": [0, 1, 4, 2, 3, 5],
        "finalStates": [4, 2, 3, 5],
        "transitions": [
          [0, "b", 1],
          [0, "a", 4],
          [1, "b", 1],
          [1, "a", 2],
          [2, "a", 2],
          [2, "b", 3],
          [3, "a", 4],
          [3, "b", 5],
          [4, "a", 4],
          [4, "b", 4],
          [5, "a", 2],
          [5, "b", 5]
        ]
      }
    }, {
      "id": "b",
      "html": "",
      "automaton": {
        "states": [0, 1, 3, 2, 4, 5],
        "finalStates": [3, 2, 4, 5],
        "transitions": [
          [0, "a", 1],
          [0, "b", 3],
          [1, "a", 1],
          [1, "b", 2],
          [2, "a", 2],
          [2, "b", 2],
          [3, "b", 3],
          [3, "a", 4],
          [4, "a", 4],
          [4, "b", 5],
          [5, "a", 4],
          [5, "b", 5]
        ]
      }
    }, {
      "id": "c",
      "html": "",
      "automaton": {
        "states": [0, 1, 3, 2, 4, 5, 9, 6, 7, 8],
        "finalStates": [3, 2, 5, 9, 6, 7, 8],
        "transitions": [
          [0, "a", 1],
          [0, "b", 3],
          [1, "a", 2],
          [4, "b", 3],
          [4, "a", 2],
          [3, "b", 3],
          [3, "a", 5],
          [2, "a", 2],
          [2, "b", 9],
          [9, "a", 9],
          [9, "b", 9],
          [5, "b", 4],
          [5, "a", 6],
          [6, "a", 6],
          [6, "b", 7],
          [7, "a", 2],
          [7, "b", 8],
          [8, "a", 6],
          [8, "b", 8]
        ]
      }
    }, {
      "id": "d",
      "html": "",
      "automaton": {
        "states": [0, 1, 2, 4, 3, 5],
        "finalStates": [2, 4, 3, 5],
        "transitions": [
          [0, "b", 1],
          [1, "a", 2],
          [1, "a", 4],
          [2, "a", 2],
          [2, "b", 3],
          [3, "a", 4],
          [3, "b", 5],
          [4, "a", 0],
          [4, "a", 4],
          [4, "b", 4],
          [5, "a", 2],
          [5, "b", 5]
        ]
      }
    }],
    "answers": ["a"],
    "singleChoice": true
  }, {
    "type": "mcq",
    "instruction": " Écrire le système d’équations associé à cet automate. Ensuite, indiquer les équations correctes parmi les suivantes. Il y a une équation correcte par état.\n",
    "isInstructionHTML": false,
    "wordingDetails": [{
      "aType": "Automaton",
      "content": "0\n1\n2\n\n3\n\n0 a 1\n1 a 2\n1 c 3\n2 b 3\n3 a 0\n3 c 1\n\n"
    }],
    "point": "1",
    "possibilities": [{
      "id": "a",
      "html": "$X_3 = aX_0 + cX_1 + \\epsilon$"
    }, {
      "id": "b",
      "html": "$X_2 = bX_3 + aX_1$"
    }, {
      "id": "c",
      "html": "$X_1 = aX_0 + \\epsilon$"
    }, {
      "id": "d",
      "html": "$X_0 = aX_1 + \\epsilon$"
    }, {
      "id": "e",
      "html": "$X_3 = aX_0 + cX_1$"
    }, {
      "id": "f",
      "html": "$X_2=bX_3+\\epsilon$"
    }, {
      "id": "g",
      "html": "$X_1 = aX_0$"
    }, {
      "id": "h",
      "html": "$X_0 = aX_1$"
    }, {
      "id": "i",
      "html": "$X_1 = aX_2 + cX_3 = \\epsilon$"
    }, {
      "id": "j",
      "html": "$X_1 = cX_2 + aX_3$"
    }, {
      "id": "k",
      "html": "$X_1=aX_2 + cX_3$"
    }, {
      "id": "l",
      "html": "$X_2 = bX_3 + aX_1 + \\epsilon$"
    }, {
      "id": "m",
      "html": "$X_2 = bX_3$"
    }],
    "answers": ["a", "h", "k", "m"],
    "singleChoice": false
  }, {
    "type": "mcq",
    "instruction": "<b>(Suite à la question précédente)</b> <br>Nous utilisons la définition de $X_2$ dans la définition de $X_1$ , puis simplifions.<br>\nL’équation que nous obtenons pour $X_1$ est :\n",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "0.5",
    "possibilities": [{
      "id": "a",
      "html": "$X_1 = (ab + c + \\epsilon)X_3$"
    }, {
      "id": "b",
      "html": "$X_1 = (ab + c)X_3 + \\epsilon$"
    }, {
      "id": "c",
      "html": "$X_1 = (ab + c + \\epsilon)X_3 + \\epsilon$"
    }, {
      "id": "d",
      "html": "$X_1 = (a + c)X_3$"
    }, {
      "id": "e",
      "html": "$X_1 = (ab + c)X_3$"
    }, {
      "id": "f",
      "html": "Aucune des équations proposées."
    }],
    "answers": ["e"],
    "singleChoice": true
  }, {
    "type": "mcq",
    "instruction": "<b>(Suite à la question précédente)</b><br>\nNous utilisons l’équation trouvée pour $X_1$ à la question précédente dans la définition de $X_3$ , puis simplifions.<br>L’équation que nous obtenons pour $X_3$ est\n",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "0.5",
    "possibilities": [{
      "id": "a",
      "html": "$X_3 = c(ab + c)X_3 + aX_0 + \\epsilon$"
    }, {
      "id": "b",
      "html": "$X_3 = c(a+c)X_3 + aX_0$"
    }, {
      "id": "c",
      "html": "$X_3=c(ab+c)X_3+aX_0$"
    }, {
      "id": "d",
      "html": "$X_3 = c(a+c)X_3 + \\epsilon$"
    }, {
      "id": "e",
      "html": "Aucune des équations proposées."
    }],
    "answers": ["a"],
    "singleChoice": true
  }, {
    "type": "mcq",
    "instruction": "<b>(Suite à la question précédente)</b><br>\nNous utilisons l’équation trouvée pour X3 à la question précédente et\nsouhaitons lui appliquer le lemme d’Arden.",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "0.75",
    "possibilities": [{
      "id": "a",
      "html": "$X_3 = (c(ab+c))^*(aX_0+\\epsilon)^*$"
    }, {
      "id": "b",
      "html": "$X_3 = (c(ab+c))^*(aX_0 + a)^*$"
    }, {
      "id": "c",
      "html": "$X_3=(c(ab+c)^*)^*(aX_0+\\epsilon)$"
    }, {
      "id": "d",
      "html": "$X_3=(c(ab+c))^*(aX_0)^*$"
    }, {
      "id": "e",
      "html": "$X_3=(c(ab+c))^*(aX_0)$"
    }, {
      "id": "f",
      "html": "$X_3=(c(ab+c))^*(aX_0+\\epsilon)$"
    }, {
      "id": "g",
      "html": "$X_3 = (c(ab+c))^*$"
    }, {
      "id": "h",
      "html": "Le lemme d’Arden ne peut pas être appliqué."
    }],
    "answers": ["f"],
    "singleChoice": true
  }, {
    "type": "mcq",
    "instruction": "<b>(Suite à la question précédente)</b><br>\nL'expression régulière associée à l'automate est :",
    "isInstructionHTML": true,
    "wordingDetails": [],
    "point": "0.25",
    "possibilities": [{
      "id": "a",
      "html": "Celle trouvée pour $X_3$"
    }, {
      "id": "b",
      "html": "Celle trouvée pour $X_0$"
    }],
    "answers": ["b"],
    "singleChoice": true
  }, {
    "type": "mcq",
    "instruction": "Considéron l'expression régulière donnée ci-dessous.<br>\nL'$\\epsilon$-AEFND équivalent à cette expression régulière est celui représenté dans :",
    "isInstructionHTML": true,
    "wordingDetails": [{
      "aType": "Regexp",
      "content": "(ad + b*c)*(cd* + aa)"
    }],
    "point": "1",
    "possibilities": [{
      "id": "a",
      "html": "",
      "automaton": {
        "states": [0, 1, 2, 3, 4, 5],
        "finalStates": [3],
        "transitions": [
          [0, "a", 1],
          [0, null, 2],
          [0, null, 3],
          [1, "d", 0],
          [2, "c", 0],
          [2, "b", 2],
          [4, "a", 3],
          [5, "d", 5],
          [5, null, 3],
          [3, "a", 4],
          [3, "c", 5]
        ]
      }
    }, {
      "id": "b",
      "html": "",
      "automaton": {
        "states": [0, 1, 5, 6, 3, 4, 2],
        "finalStates": [2],
        "transitions": [
          [0, null, 1],
          [0, "a", 5],
          [0, null, 6],
          [1, "a", 3],
          [1, "c", 4],
          [3, "a", 2],
          [4, "d", 4],
          [4, null, 2],
          [5, "d", 0],
          [6, "c", 0],
          [6, "b", 6]
        ]
      }
    }, {
      "id": "c",
      "html": "",
      "automaton": {
        "states": [0, 2, 3, 1, 4, 5],
        "finalStates": [0, 1],
        "transitions": [
          [0, "a", 2],
          [0, null, 3],
          [0, null, 1],
          [2, "d", 0],
          [3, "c", 0],
          [3, "b", 3],
          [4, "a", 1],
          [5, "d", 5],
          [5, null, 1],
          [1, "a", 4],
          [1, "c", 5]
        ]
      }
    }, {
      "id": "d",
      "html": "",
      "automaton": {
        "states": [0, 2, 3, 1, 4, 5],
        "finalStates": [0, 1],
        "transitions": [
          [0, "a", 2],
          [0, "b", 3],
          [0, null, 1],
          [2, "d", 0],
          [3, "c", 0],
          [3, "b", 3],
          [4, "d", 4],
          [4, "d", 1],
          [5, "a", 1],
          [1, "c", 4],
          [1, "a", 5]
        ]
      }
    }, {
      "id": "e",
      "html": "",
      "automaton": {
        "states": [0, 1, 2, 3, 5, 4, 6],
        "finalStates": [3, 4],
        "transitions": [
          [0, "a", 1],
          [0, null, 2],
          [0, null, 3],
          [1, "d", 0],
          [2, "c", 0],
          [2, "b", 2],
          [5, "a", 4],
          [6, "d", 6],
          [6, null, 4],
          [3, "a", 5],
          [3, "c", 6]
        ]
      }
    }, {
      "id": "f",
      "html": "",
      "automaton": {
        "states": [0, 1, 2, 3, 5, 6, 4],
        "finalStates": [4],
        "transitions": [
          [0, "a", 1],
          [0, "b", 2],
          [0, null, 3],
          [1, "d", 0],
          [2, "c", 0],
          [2, "b", 2],
          [3, "a", 5],
          [3, "c", 6],
          [5, "a", 4],
          [6, "d", 6],
          [6, "d", 4]
        ]
      }
    }],
    "answers": ["b"],
    "singleChoice": true
  }, {
    "type": "mcq",
    "instruction": "Considérons l’expression régulière suivante.<br>\nL’$\\epsilon$-AEFND résultant de la traduction compositionnelle d’expressions régulières vers automates est celui représenté dans :\n",
    "isInstructionHTML": false,
    "wordingDetails": [{
      "aType": "Regexp",
      "content": "( a+b )*( cd )"
    }],
    "point": "1",
    "possibilities": [{
      "id": "a",
      "html": "",
      "automaton": {
        "states": [0, 1, 2, 3, 6, 4, 7, 5, 8, 9],
        "finalStates": [9],
        "transitions": [
          [0, null, 1],
          [0, null, 2],
          [1, "a", 3],
          [2, null, 6],
          [3, null, 4],
          [6, "c", 7],
          [4, "b", 5],
          [5, null, 1],
          [5, null, 2],
          [7, null, 8],
          [8, "d", 9]
        ]
      }
    }, {
      "id": "b",
      "html": "",
      "automaton": {
        "states": [0, 7, 2, 1, 3, 4, 5, 6, 8, 9, 10, 11],
        "finalStates": [0, 2, 11],
        "transitions": [
          [0, null, 7],
          [0, null, 2],
          [1, "a", 3],
          [3, null, 4],
          [4, null, 7],
          [4, null, 2],
          [5, null, 4],
          [6, "b", 5],
          [7, null, 1],
          [7, null, 6],
          [8, "c", 9],
          [9, null, 10],
          [10, "d", 11],
          [2, null, 8]
        ]
      }
    }, {
      "id": "c",
      "html": "",
      "automaton": {
        "states": [0, 2, 7, 1, 3, 8, 4, 5, 6, 9, 10, 11],
        "finalStates": [11],
        "transitions": [
          [0, null, 2],
          [0, null, 7],
          [1, "a", 3],
          [2, null, 8],
          [3, null, 4],
          [4, null, 2],
          [4, null, 7],
          [5, null, 4],
          [6, "b", 5],
          [7, null, 1],
          [7, null, 6],
          [8, "c", 9],
          [9, null, 10],
          [10, "d", 11]
        ]
      }
    }, {
      "id": "d",
      "html": "",
      "automaton": {
        "states": [0, 7, 2, 1, 6, 3, 4, 5, 8, 9, 10, 11],
        "finalStates": [0, 2, 11],
        "transitions": [
          [0, null, 7],
          [0, null, 2],
          [7, null, 1],
          [7, null, 6],
          [1, "a", 3],
          [3, null, 4],
          [4, null, 7],
          [4, null, 2],
          [5, null, 4],
          [6, "b", 5],
          [8, "c", 9],
          [9, null, 10],
          [10, "d", 11],
          [2, null, 8],
          [2, null, 11]
        ]
      }
    }, {
      "id": "e",
      "html": "",
      "automaton": {
        "states": [0, 2, 7, 1, 3, 8, 11, 4, 5, 6, 9, 10],
        "finalStates": [0, 11],
        "transitions": [
          [0, null, 2],
          [0, null, 7],
          [1, "a", 3],
          [2, null, 8],
          [2, null, 11],
          [3, null, 4],
          [4, null, 2],
          [4, null, 7],
          [5, null, 4],
          [6, "b", 5],
          [7, null, 1],
          [7, null, 6],
          [8, "c", 9],
          [9, null, 10],
          [10, "d", 11]
        ]
      }
    }, {
      "id": "f",
      "html": "Aucune des figures proposées."
    }],
    "answers": ["c"],
    "singleChoice": true
  }, {
    "type": "textInput",
    "instruction": "Quelle est la constante d'itération minimale du langage dénoté par l'expression régulière suivante :",
    "isInstructionHTML": false,
    "wordingDetails": [{
      "aType": "Regexp",
      "content": "(a* + b)ba(b* + b + ε)"
    }],
    "point": "1",
    "correctAnswers": ["4"],
    "inputPlaceholder": "Input your answer here"
  }]
}