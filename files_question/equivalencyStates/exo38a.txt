0
1

2
3

0 b 0
0 a 1
1 a 1
1 b 2
2 b 2
2 a 3
3 b 2
3 a 3

<representation type='image/svg+xml'>
<svg width="1639" height="906" viewBox="0 0 1639 906" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g class="graph" data-id="graph0">
<title>automaton</title>



<g class="node" data-id="MA==">
<title>0</title>
<ellipse fill="white" stroke="#000000" cx="133.6416" cy="60.8" rx="18" ry="18" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="133.6416" y="65" font-family="Times,serif" font-size="14.00" fill="#000000">0</text>
</g>

<g class="edge" data-id="initialStateArrow">
<title>_begin-&gt;0</title>
<path fill="none" stroke="#000000" d="M77.6416015625,60.79999923706055C96.97493489583333,60.79999923706055 106.30826822916667,60.79999923706055 105.6416015625,60.79999923706055"></path>
<polygon fill="#000000" stroke="#000000" points="115.6416015625,60.79999923706055 105.6416015625,56.79999923706055 110.6416015625,60.79999923706055 105.6416015625,60.79999923706055 105.6416015625,60.79999923706055 105.6416015625,60.79999923706055 110.6416015625,60.79999923706055 105.6416015625,64.79999923706055 115.6416015625,60.79999923706055 115.6416015625,60.79999923706055"></polygon>
</g>

<g class="edge" data-id="MA== MA==">
<title>0-&gt;0</title>
<path fill="none" stroke="black" d="M 127.2623 43.7627 C 125.9605 33.9421 128.0869 24.800000000000004 133.6416 24.800000000000004 C 137.1132 24.800000000000004 139.2457 28.3711 140.0391 33.4471"></path>
<polygon fill="black" stroke="black" points="143.539,33.769 140.021,43.7627 136.539,33.7565 143.539,33.769"></polygon>
<text text-anchor="middle" x="133.6416" y="20.6" font-family="Times,serif" font-size="14.00">b</text>
</g>

<g class="node" data-id="MQ==">
<title>1</title>
<ellipse fill="white" stroke="#000000" cx="211.8562" cy="60.8" rx="18" ry="18" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="211.8562" y="65" font-family="Times,serif" font-size="14.00" fill="#000000">1</text>
</g>

<g class="edge" data-id="MA== MQ==">
<title>0-&gt;1</title>
<path fill="none" stroke="black" d="M 151.7822 60.8 C 161.259 60.8 173.068 60.8 183.7018 60.8"></path>
<polygon fill="black" stroke="black" points="183.814,57.2999 193.814,60.8 183.814,64.2999 183.814,57.2999"></polygon>
<text text-anchor="middle" x="172.7489" y="56.6" font-family="Times,serif" font-size="14.00">a</text>
</g>

<g class="edge" data-id="MQ== MQ==">
<title>1-&gt;1</title>
<path fill="none" stroke="black" d="M 205.1225 43.7627 C 203.7483 33.9421 205.9929 24.800000000000004 211.8562 24.800000000000004 C 215.5207 24.800000000000004 217.7717 28.3711 218.6091 33.4471"></path>
<polygon fill="black" stroke="black" points="222.109,33.7693 218.59,43.7627 215.109,33.7561 222.109,33.7693"></polygon>
<text text-anchor="middle" x="211.8562" y="20.6" font-family="Times,serif" font-size="14.00">a</text>
</g>

<g class="node" data-id="Mg==">
<title>2</title>
<ellipse fill="white" stroke="#000000" cx="294.8562" cy="60.8" rx="18" ry="18" fill-opacity="0"></ellipse>
<ellipse fill="white" stroke="#000000" cx="294.8562" cy="60.8" rx="22" ry="22" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="294.8562" y="65" font-family="Times,serif" font-size="14.00" fill="#000000">2</text>
</g>

<g class="edge" data-id="MQ== Mg==">
<title>1-&gt;2</title>
<path fill="none" stroke="black" d="M 229.8659 60.8 C 239.4044 60.8 251.4116 60.8 262.5018 60.8"></path>
<polygon fill="black" stroke="black" points="262.743,57.2999 272.743,60.8 262.743,64.2999 262.743,57.2999"></polygon>
<text text-anchor="middle" x="251.3562" y="56.6" font-family="Times,serif" font-size="14.00">b</text>
</g>

<g class="edge" data-id="Mg== Mg==">
<title>2-&gt;2</title>
<path fill="none" stroke="black" d="M 287.1734 39.8093 C 286.2249 29.7128 288.7858 20.800000000000004 294.8562 20.800000000000004 C 298.745 20.800000000000004 301.1935 24.4578 302.2018 29.7414"></path>
<polygon fill="black" stroke="black" points="305.702,29.6977 302.539,39.8093 298.706,29.932 305.702,29.6977"></polygon>
<text text-anchor="middle" x="294.8562" y="16.599999999999994" font-family="Times,serif" font-size="14.00">b</text>
</g>

<g class="node" data-id="Mw==">
<title>3</title>
<ellipse fill="white" stroke="#000000" cx="381.8562" cy="60.8" rx="18" ry="18" fill-opacity="0"></ellipse>
<ellipse fill="white" stroke="#000000" cx="381.8562" cy="60.8" rx="22" ry="22" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="381.8562" y="65" font-family="Times,serif" font-size="14.00" fill="#000000">3</text>
</g>

<g class="edge" data-id="Mg== Mw==">
<title>2-&gt;3</title>
<path fill="none" stroke="black" d="M 317.2608 60.8 C 327.0784 60.8 338.7876 60.8 349.5234 60.8"></path>
<polygon fill="black" stroke="black" points="349.829,57.2999 359.829,60.8 349.829,64.2999 349.829,57.2999"></polygon>
<text text-anchor="middle" x="338.3562" y="56.6" font-family="Times,serif" font-size="14.00">a</text>
</g>

<g class="edge" data-id="Mw== Mg==">
<title>3-&gt;2</title>
<path fill="none" stroke="black" d="M 364.5243 74.596 C 355.869 79.87650000000001 345.0842 84.10040000000001 334.8562 81.6 C 330.8644 80.6242 326.8276 79.1738 322.9322 77.4846"></path>
<polygon fill="black" stroke="black" points="321.082,80.4752 313.646,72.9277 324.165,74.1911 321.082,80.4752"></polygon>
<text text-anchor="middle" x="338.3562" y="77.6" font-family="Times,serif" font-size="14.00">b</text>
</g>

<g class="edge" data-id="Mw== Mw==">
<title>3-&gt;3</title>
<path fill="none" stroke="black" d="M 374.1734 39.8093 C 373.2249 29.7128 375.7858 20.800000000000004 381.8562 20.800000000000004 C 385.745 20.800000000000004 388.1935 24.4578 389.2018 29.7414"></path>
<polygon fill="black" stroke="black" points="392.702,29.6977 389.539,39.8093 385.706,29.932 392.702,29.6977"></polygon>
<text text-anchor="middle" x="381.8562" y="16.599999999999994" font-family="Times,serif" font-size="14.00">a</text>
</g>
</g>
</svg>
</representation>
