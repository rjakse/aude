0
1
2
4

0
3

0 a 1
1 a 2
4 b 3
3 b 2
3 a 4
1 b 3

<representation type='image/svg+xml'>
<svg width="1639" height="906" viewBox="0 0 1639 906" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g class="graph" data-id="graph0">
<title>automaton</title>



<g class="node" data-id="MA==">
<title>0</title>
<ellipse fill="white" stroke="#000000" cx="137.6416" cy="22" rx="18" ry="18" fill-opacity="0"></ellipse><ellipse fill="white" stroke="#000000" cx="137.6416" cy="22" rx="22" ry="22" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="137.6416015625" y="26" font-family="Times,serif" font-size="14.00" fill="#000000">0</text>
</g>

<g class="edge" data-id="initialStateArrow">
<title>_begin-&gt;0</title>
<path fill="none" stroke="#000000" d="M77.6416015625,22C96.97493489583333,22 106.30826822916667,22 105.6416015625,22"></path>
<polygon fill="#000000" stroke="#000000" points="115.6416015625,22 105.6416015625,18 110.6416015625,22 105.6416015625,22 105.6416015625,22 105.6416015625,22 110.6416015625,22 105.6416015625,26 115.6416015625,22 115.6416015625,22"></polygon>
</g>

<g class="node" data-id="MQ==">
<title>1</title>
<ellipse fill="white" stroke="#000000" cx="220.6416" cy="62" rx="18" ry="18" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="220.6416" y="66.2" font-family="Times,serif" font-size="14.00" fill="#000000">1</text>
</g>

<g class="edge" data-id="MA== MQ==">
<title>0-&gt;1</title>
<path fill="none" stroke="black" d="M 157.62522530613595 31.20080333844767 C 163.2442 33.81979999999999 174.5856 39.1335 184.6416 44 C 188.1167 45.68180000000001 191.7687 47.476 195.3593 49.2559"></path>
<polygon fill="black" stroke="black" points="197.172,46.2491 204.557,53.8465 194.046,52.5123 197.172,46.2491"></polygon>
<text text-anchor="middle" x="181.1416" y="36.80000000000001" font-family="Times,serif" font-size="14.00">a</text>
</g>

<g class="node" data-id="Mg==">
<title>2</title>
<ellipse fill="white" stroke="#000000" cx="298.8562" cy="159" rx="18" ry="18" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="298.8562" y="163.2" font-family="Times,serif" font-size="14.00" fill="#000000">2</text>
</g>

<g class="edge" data-id="MQ== Mg==">
<title>1-&gt;2</title>
<path fill="none" stroke="black" d="M 232.1824 76.3127 C 245.0799 92.3078 266.1162 118.39670000000001 281.1096 136.99110000000002"></path>
<polygon fill="black" stroke="black" points="283.97,134.962 287.522,144.944 278.521,139.356 283.97,134.962"></polygon>
<text text-anchor="middle" x="259.7489" y="103.8" font-family="Times,serif" font-size="14.00">a</text>
</g>

<g class="node" data-id="NA==">
<title>4</title>
<ellipse fill="white" stroke="#000000" cx="220.6416" cy="119" rx="18" ry="18" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="220.6416" y="123.2" font-family="Times,serif" font-size="14.00" fill="#000000">4</text>
</g>

<g class="node" data-id="Mw==">
<title>3</title>
<ellipse fill="white" stroke="#000000" cx="137.6416" cy="122" rx="18" ry="18" fill-opacity="0"></ellipse>
<ellipse fill="white" stroke="#000000" cx="137.6416" cy="122" rx="22" ry="22" fill-opacity="0"></ellipse>
<text text-anchor="middle" x="137.6416" y="126.2" font-family="Times,serif" font-size="14.00" fill="#000000">3</text>
</g>

<g class="edge" data-id="NA== Mw==">
<title>4-&gt;3</title>
<path fill="none" stroke="black" d="M 206.4234 130.0752 C 200.1182 134.3099 192.3828 138.6388 184.6416 140.8 C 178.1979 142.59890000000001 171.4305 141.66559999999998 165.1016 139.4659"></path>
<polygon fill="black" stroke="black" points="163.294,142.49 155.627,135.178 166.18,136.112 163.294,142.49"></polygon>
<text text-anchor="middle" x="181.1416" y="135.8" font-family="Times,serif" font-size="14.00">b</text>
</g>



<g class="edge" data-id="Mw== Mg==">
<title>3-&gt;2</title>
<path fill="none" stroke="black" d="M 154.5433 136.3615 C 166.9571 145.9219 184.7163 157.6403 202.6416 162.8 C 225.0066 169.2376 251.5013 167.5496 271.0196 164.6698"></path>
<polygon fill="black" stroke="black" points="270.635,161.186 281.075,162.991 271.788,168.09 270.635,161.186"></polygon>
<text text-anchor="middle" x="220.6416" y="157.8" font-family="Times,serif" font-size="14.00">b</text>
</g>

<g class="edge" data-id="Mw== NA==">
<title>3-&gt;4</title>
<path fill="none" stroke="black" d="M 159.884 121.1961 C 169.8959 120.83420000000001 181.8293 120.4029 192.4588 120.0187"></path>
<polygon fill="black" stroke="black" points="192.414,116.518 202.534,119.654 192.667,123.514 192.414,116.518"></polygon>
<text text-anchor="middle" x="181.1416" y="116.8" font-family="Times,serif" font-size="14.00">a</text>
</g>
<g data-id="MQ== Mw==" class="edge"><title>1-&gt;3</title><path fill="none" stroke="black" d="M 206.05401135167273 72.54524593553779 C 191.89437265799427 82.78112932855835 177.73473396431584 93.01701272157892 163.57509527063738 103.25289611459948"></path><polygon fill="black" stroke="black" points="165.626,106.089 155.471,109.111 161.525,100.416 165.626,106.089"></polygon><text text-anchor="middle" font-family="Times Roman,serif" font-size="14.00" x="184.81455331115507" y="82.89907102506864">b</text></g></g>
</svg>
</representation>
