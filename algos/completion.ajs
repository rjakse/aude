export function complete(A, alphabet)
    let compA := new Automaton
    let trans := A.getTransitionFunction()

    alphabet := (
        if alphabet then
            alphabet union A.getAlphabet()
        else
            A.getAlphabet()
    )

   compA.setInitialState(A.getInitialState())

   let sink := 'S'

   while A.hasState(sink) do
      sink += 'S'
   done

   foreach state in A.getStates() do
      foreach symbol in alphabet do
         if trans(state, symbol) is empty then
            compA.addState(sink)
            compA.addTransition(state, symbol, sink)
         end if
      done
   done

   if compA.hasState(sink) then
      foreach a in alphabet do
         compA.addTransition(sink, a, sink)
      done
   end if

   foreach state in A.getFinalStates() do
      compA.setFinalState(state)
   done

   foreach t in A.getTransitions() do
      compA.addTransition(t)
   done

   return compA
end function

export function isCompleted(A)
    let numStateBefore := A.getStates().card()
    let numStateAfter := complete(A).getStates().card()
    return (numStateBefore = numStateAfter)
end function

run(complete, get_automaton(currentAutomaton))
